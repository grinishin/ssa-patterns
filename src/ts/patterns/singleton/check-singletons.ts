import {SingletonClass, SingletonFunction} from "./singleton-function";
import {SingletonTSClass} from "./singleton-class";

export let CheckSingletons = () => {
    let sinletonFunction_1 = new SingletonFunction();
    let sinletonFunction_2 = new SingletonFunction();
    let sinletonFunctionChecked = sinletonFunction_1 == sinletonFunction_2;


    let SingletonClass_1 = new SingletonClass();
    let SingletonClass_2 = new SingletonClass();
    let SingletonClassChecked = SingletonClass_1 == SingletonClass_2;

    let SingletonTSClass_1 = new SingletonTSClass();
    let SingletonTSClass_2 = new SingletonTSClass();
    let SingletonTSClassChecked = SingletonTSClass_1 == SingletonTSClass_2;

    return sinletonFunctionChecked && SingletonClassChecked && SingletonTSClassChecked
};