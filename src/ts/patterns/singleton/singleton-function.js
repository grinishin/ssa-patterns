export function SingletonFunction(){
    if (typeof SingletonFunction.instance === 'object') {
        return SingletonFunction.instance
    }
    this.method = function () {
        console.log(`method`)
    };

    this.prototypeMethod();
    SingletonFunction.instance = this;
    return this;
}

SingletonFunction.prototype.prototypeMethod = function () {
    // this.method()
};



export function SingletoneInScope() {
    let instance;
    SingletoneInScope = function SingletoneInScope() {
        return instance;
    };

    SingletoneInScope.prototype = this;

    instance = new SingletoneInScope();
    instance.constructor = SingletoneInScope;

    this.field = 5;
}


export class SingletonClass {
    _instance;

    constructor(){
        if (SingletonClass._instance) return SingletonClass._instance;

        SingletonClass._instance = this;
    }

    foeld_1 = 5;
    foeld_2 = 15;

    method(){
        console.log('SingletonClass mathod')
    }
}
