export class SingletonTSClass {
    private static _instance: SingletonTSClass;
    constructor(){
        if (SingletonTSClass._instance) return SingletonTSClass._instance;

        SingletonTSClass._instance = this;
    }

    foeld_1 = 5;
    foeld_2 = 15;

    method(){
        console.log('SingletonClass mathod')
    }
}