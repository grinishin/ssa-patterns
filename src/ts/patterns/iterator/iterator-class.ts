// import 'rxjs/add/observable/of';
// import 'rxjs/add/observable/forkJoin';
// import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/fromPromise';
// import 'rxjs/add/operator/take';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/filter';
// import 'rxjs/add/operator/withLatestFrom';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export interface IteratedObject {
    next: IteratorReturn;
    current: IteratorElement | Promise<IteratorElement> | Observable<IteratorElement>;
    prev: IteratorReturn;
    toFirst: IteratorReturn;
    toLast: IteratorReturn;
    hasNext: boolean;
    hasPrev: boolean;
}

type IteratorReturn = IteratorElement | Promise<IteratorElement> | Observable<IteratorElement>;

interface IteratorElement {
    element: any;
    index: number;
}

export enum IteratorType {
    PRIMITIVE = <any>'0',
    PROMISE = <any>'1',
    OBSERVABLE = <any>'2'
}

export class Iterator implements IteratedObject {
    private data: any[];
    private type: IteratorType;
    private index: number = 0;


    // private set data(data: any[]) {
    //     this._data = data
    // }
    // private get data(): any[] {
    //     return this._data
    // }
    // private set type(type: IteratorType) {
    //     this._type = type
    // }
    // private get type(): IteratorType {
    //     return this._type
    // }

    constructor(private data$: any, private type$: IteratorType) {
        if (Array.isArray(data$)) {
            this.data = data$.slice()
        } else if (typeof data$ === 'object') {
            this.data = Object.keys(data$).map(key => Object.create({key, object: data$[key]}))
        } else {
            throw new Error('Unresolved data type')
        }
        this.type = type$;
    }

    get next(): IteratorElement | Promise<IteratorElement> | Observable<IteratorElement> {
        if (this.hasNext) {
            this.index++;
            return this.coverResponseToType()
        } else {
            throw new Error('There is no next element')
        }
    }

    get current(): IteratorElement | Promise<IteratorElement> | Observable<IteratorElement> {
        return this.coverResponseToType()
    }

    get prev(): IteratorElement | Promise<IteratorElement> | Observable<IteratorElement> {
        if (this.hasPrev) {
            this.index--;
            return this.coverResponseToType()
        } else {
            throw new Error('There is no prev element')
        }
    }


    get toFirst(): IteratorElement | Promise<IteratorElement> | Observable<IteratorElement> {
        this.index = 0;
        return this.coverResponseToType()
    }

    get toLast(): IteratorElement | Promise<IteratorElement> | Observable<IteratorElement> {
        this.index = this.data.length - 1;
        return this.coverResponseToType()
    }

    get hasNext(): boolean {
        return this.index < this.data.length - 1
    }

    get hasPrev(): boolean {
        return this.index > 0
    }

    private coverResponseToType(): IteratorElement | Promise<IteratorElement> | Observable<IteratorElement> {
        const toReturn: IteratorElement = {
            element: this.data[this.index],
            index: this.index
        };
        switch (this.type) {
            case IteratorType.PRIMITIVE:
                return toReturn;
            case IteratorType.PROMISE:
                return new Promise((resolve) => {
                    resolve(toReturn)
                });
            case IteratorType.OBSERVABLE:
                return Observable.of(toReturn)
        }
    }

    static of(data: any, type: IteratorType = IteratorType.PRIMITIVE) {
        let iterator = new Iterator(data, type);
        delete iterator.data$;
        delete iterator.type$;
        return iterator
    }
}

export function iteratorUsing() {
    let array = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
    let arrayIterator = Iterator.of(array, IteratorType.PROMISE);
    let arrayIterator2 = Iterator.of(array, IteratorType.OBSERVABLE);

    do {
        (<Promise<IteratorElement>>arrayIterator.next).then(
            (res: IteratorElement) => {
                console.log(res.element)
            }
        )
    } while (arrayIterator.hasNext);

    do {
        (<Observable<IteratorElement>>arrayIterator2.next).subscribe(
            (res: IteratorElement) => {
                console.log(res.element + 777)
            }
        )
    } while (arrayIterator2.hasNext)
}