let scoreBoard = {
    element: document.getElementById('results'),
    update: function (score: any) {
        let i, msg = '';
        for (i in score) {
            if (score.hasOwnProperty(i)) {
                msg += `<p><strong>${i}</strong>: ${score[i]}</p>`
            }
        }
        this.element.innerHTML = msg;
    }
};

let mediator = {
    players: {},
    setup: function () {
        let players = this.players;
        players.left = new Player('Left');
        players.right = new Player('Right');
    },
    played: function () {
        let players = this.players,
            score = {
                Left: players.left.points,
                Right: players.right.points
            };
        scoreBoard.update(score)
    },
    keyPress: (e: any) => {
        let players: any = mediator.players;
        if (e.which === 49) {
            players.left.play();
        } else if (e.which === 48) {
            players.right.play();
        }
    },
    winner: () => {
        let players: any = mediator.players;
        return players.left.points == players.right.points ? 'Friendship' : players.left.points > players.right.points ? players.left.name : players.right.name
    }
};

export class Player {
    points = 0;
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    play() {
        this.points += 1;
        mediator.played();
    }
}

export function mediatorGame() {
    mediator.setup();
    window.onkeypress = mediator.keyPress;

    setTimeout(() => {
        window.onkeypress = null;
        alert(`Game over!\nthe winner is ${mediator.winner()}`)
    }, 5000)
}