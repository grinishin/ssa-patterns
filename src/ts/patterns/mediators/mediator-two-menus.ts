import {bands} from "./data";

class Menu {
    name: string;
    collectionObject: any = {};
    mediator: any;

    constructor(mediator: any, name: string, collection: HTMLCollection | NodeListOf<Element>) {
        this.name = name;
        this.mediator = mediator;
        this.init(collection);
    }

    init(collection: HTMLCollection | NodeListOf<Element>) {
        this.collectionToObject(collection);
        this.setInMediator();
    }

    setInMediator() {
        this.mediator.menus[this.name] = this;
    }

    collectionToObject(collection: HTMLCollection | NodeListOf<Element>) {
        Array.prototype.forEach.call(collection, (elem: HTMLElement, index: number, array: HTMLElement[]) => {
            this.collectionObject[elem.innerText] = {
                elem,
                next: array[index + 1] ? array[index + 1].innerText : array[0].innerText,
                prev: array[index - 1] ? array[index - 1].innerText : array[array.length - 1].innerText
            };
            this.addEventListeners(elem);
        })
    }

    addEventListeners(elem: HTMLElement) {
        elem.dataset.letter = elem.innerText;
        elem.addEventListener('click', (e: Event) => {
            let element = (<HTMLElement>e.target),
                letter = element.innerHTML;
            this.mediator.click(letter)
        })
    }
}

let field = {
    textArea: document.getElementById('field'),
    update: function (names: string[]) {
        this.textArea.innerHTML = `<p>${names.join('</p><p>')}</p>`;
    },
    next: document.getElementById('next'),
    prev: document.getElementById('prev')
};

let mediator = {
    cache: {},
    activeLetter: 'A',
    menus: {},
    init: function () {
        this.addEventListeners();
        this.click(this.activeLetter);
    },
    clickOn: function (letter: string) {
        if (!this.cache[letter]) {
            this.cache[letter] = bands.filter(band => band.toUpperCase()[0] === letter.toUpperCase()).sort()
        }
        this.activeLetter = letter;
        field.update(this.cache[letter]);
    },
    addEventListeners: function () {
        field.next.addEventListener('click', () => {
            this.next()
        });
        field.prev.addEventListener('click', () => {
            this.prev()
        });
    },
    next: function () {
        this.click(this.menus.top.collectionObject[this.activeLetter].next)
    },
    prev: function () {
        this.click(this.menus.top.collectionObject[this.activeLetter].prev)
    },
    click: function (letter: string) {
        for (let menu in this.menus){
            this.menus[menu].collectionObject[this.activeLetter].elem.style.color = '';
            let elem = this.menus[menu].collectionObject[letter].elem;
            elem.style.color = 'red';
        }
        this.clickOn(letter);
    }
};

export function mediatorTwoMenus() {
    const topMenu = new Menu(mediator, 'top', document.querySelector('.horizontal').children);
    const leftMenu = new Menu(mediator, 'left', document.querySelectorAll('.tbody tr > td:first-child'));
    mediator.init();
}