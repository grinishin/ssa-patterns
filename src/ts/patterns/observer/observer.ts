

class Observer {
    private subscribers: Map<Function, string> = new Map();

    constructor(){
    }

    static toObservable(object :any){
        let observer: any = new Observer();
        object.publish = observer.publish;
        object.subscribe = observer.subscribe;
        object.unsubscribe = observer.unsubscribe;
        return Object.assign(object, observer)
    }

    static of(){

    }

    subscribe(fn: Function, type: string = 'any'){
        this.subscribers.set(fn, type)
    }

    publish(arg: any, type: string = 'any'){
        this.subscribers.forEach((value: string, key: Function) => {
            if (!!type && value === type) key(arg);
        })
    }

    unsubscribe(fn: Function){
        this.subscribers.delete(fn)
    }
}

export function observerRun(){
    let publisher: any = {
        some: function () {
            this.publish('aaa')
        }
    };
    Observer.toObservable(publisher);

    let subscriber = function(){
        console.log('I am subscriber', arguments);
    };
    publisher.subscribe(subscriber);
    publisher.some();
}