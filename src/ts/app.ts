import {checkDecoratedClass} from "./ts-decorators/for-class";
import {CheckSingletons} from "./patterns/singleton/check-singletons";
import {checkDecoratedFunction} from "./ts-decorators/for-method";
import {checkDecoratedSetterGetter} from "./ts-decorators/for-det-get";
import {iteratorUsing} from "./patterns/iterator/iterator-class";
import {mediatorGame} from "./patterns/mediators/mediator-game";
import {mediatorTwoMenus} from "./patterns/mediators/mediator-two-menus";
import {observerRun} from "./patterns/observer/observer";

(function(){
    console.log(`CheckSingletons == ${CheckSingletons()}`);

    // checkDecoratedClass();
    // checkDecoratedFunction();
    // checkDecoratedSetterGetter();
    // iteratorUsing();
    // mediatorGame();

    window.addEventListener('DOMContentLoaded', function () {
        mediatorTwoMenus();
        observerRun();
    })
})();