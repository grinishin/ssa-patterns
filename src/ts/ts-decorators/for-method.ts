// // import {logMethod} from "ts-log-decorator/dist";
//
// export function log(type?: string): any {
//     switch (type) {
//         case 'c':
//         case 'class': {
//             return logClass
//         }
//         case 'p':
//         case 'param': {
//             return logParam
//         }
//         case 'm':
//         case 'method':
//         case 'function':
//         case 'f': {
//             return logMethod
//         }
//         default: {
//             return defineAutomaticaly
//         }
//     }
// }
//
// export function defineAutomaticaly() {
//     const args = arguments;
//     if (args.length === 1) {
//         logClass.apply(this, args);
//         return;
//     }
//     if (args[2] || args[2] === 0){
//         switch (typeof args[2]) {
//             case 'object' : {
//                 logMethod.apply(this, args);
//                 return
//             }
//             default: {
//                 logParam.apply(this, args);
//                 return
//             }
//         }
//     }
//     throw new Error('Somithing went wrong with package \'ts-log-decorator\', try to specify the type')
// }
//
// export function logMethod(target: any, propertyName: string, descriptor: PropertyDescriptor) {
//     let oldT = target;
//     let originalMethod = descriptor.value;
//     descriptor.value = function () {
//         const args: any = arguments;
//         let returnValue = originalMethod.apply(this, arguments);
//         let stringArgsArray = Array.prototype.map.call(args, (arg: any) => `%c${JSON.stringify(arg)}%c: "${typeof arg}"%c`);
//         const stringArgs = stringArgsArray.join(', ');
//
//         let stylesArray = [];
//         for (let i = 0, max = stringArgsArray.length * 3; i < max; i++) {
//             switch (i % 3) {
//                 case 0: {
//                     stylesArray.push('font-style: normal; font-weight: bold; color: blue; font-size: 12px');
//                     break;
//                 }
//                 case 1 : {
//                     stylesArray.push('font-style: italic;');
//                     break;
//                 }
//                 case 2 : {
//                     stylesArray.push('font-style: normal; color: black; font-weight: normal');
//                     break;
//                 }
//             }
//         }
//
//         if (typeof returnValue == 'undefined') {
//             returnValue = 'void'
//         } else {
//             returnValue = `%c${JSON.stringify(returnValue)}%c: "${Array.isArray(returnValue) ? 'Array' : typeof returnValue}"`
//         }
//         console.log.apply(null, [`method %c${propertyName}%c = (${stringArgs}) => ${returnValue}`, 'color: red; font-size: 14px', 'color: black;', ...stylesArray, 'font-style: normal; font-weight: bold; color: green; font-size: 12px', 'font-style: italic;']);
//         return returnValue;
//     }
// }
//
// function logClass(target: Function) {
//     console.log(`%cInstance of class %c${target.name}%c has been created.\n`, 'font-style: italic', 'font-style: normal; font-weight: bold; color: green; font-size: 15px', 'font-style: italic');
// }
//
//
// function logParam(target: any, key: string, index: number) {
//     if (index > 5) {
//         console.log('Are you crazy to use so many parameters???', 'color: red, font-size: 30px')
//     }
//     let getNumber = (index: number) => {
//         const numberByOrder: number = index + 1;
//         let suff: string = numberByOrder == 1 ? 'st' : numberByOrder == 2 ? 'nd' : numberByOrder == 3 ? 'd' : 'th';
//
//         return `${numberByOrder}%c${suff}%c`
//     };
//
//     const data = `%cclass %c${target.constructor.name ? target.constructor.name : target.prototype.name}%c, method %c${key}%c, ${getNumber(index)} param`;
//     const styles = [
//         'font-style: italic', 'font-style: normal; font-weight: bold; color: green; font-size: 15px',
//         'font-style: italic', 'font-style: normal; font-weight: normal; color: red; font-size: 15px',
//         '',
//         'font-style: italic; font-size: 8px', 'font-style: normal',
//     ];
//     console.log.apply(null, [data, ...styles])
// }


import {log} from "ts-log-decorator";

@log('c')
class User {

    value: number = 0;
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    // @log('m')
    @log()
    print(@log() name: string, asd: any): any {
        return [
            (name ? name : this.name),
            asd
        ]
    }

    set setNewName(name: string) {
        this.name = name
    }
}

export function checkDecoratedFunction() {
    let tom = new User("Tom");
    tom.print('asdas', 111);  // Tom
    tom.setNewName = ('111');
    console.dir(tom)
}