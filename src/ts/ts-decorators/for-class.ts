function HidePublicProp<TFunction extends Function>(target: TFunction): TFunction {
    console.log("sealed decorator");
    let stored = target;
    let f: Function = function(name: string){
        this.print = () => {
            console.log(`My name is ${name}`);
        };
    };

    console.warn(`Мы переопределяем конструктор, но теряем принцип наследования,\nто есть каждый потомок задекорированого класа User уже не будет экземпляром этого класса`);
    return <TFunction>f
}


@HidePublicProp
export class User {
    name: string;
    constructor(name: string = 'User'){
        this.name = name;
    }
    print():void{
        console.log(this.name);
    }
}

export function checkDecoratedClass(){
    let user = new User('11111111111');
    // user.print();
    // console.log(user)
}

