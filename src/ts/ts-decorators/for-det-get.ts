function NumbersInStringOnly(target: any, propertyKey: string, descriptor: PropertyDescriptor){
    target; propertyKey;
    const oldSetter = descriptor.set;
    descriptor.set = function (numbers: string) {
        let reg = /^[0-9]+$/,
            withoutSpaces: string = numbers.replace(/\s/g, '');
        if (reg.exec(withoutSpaces)){
            oldSetter.call(this, withoutSpaces)
        } else {
            throw new Error('You are trying to change phone number to new one that has chars or signs')
        }
    }
}


class User{
    private _phone: string;
    name: string;

    constructor(name: string){
        this.name = name
    }

    @NumbersInStringOnly
    set phone(phone: string){
        this._phone = phone;
    }

    get phone(): string{
        return this._phone;
    }
}

export function checkDecoratedSetterGetter(){
    let user = new User('Oleh');
    user.phone = '054 16 8546';
    // console.log(user.phone)
}